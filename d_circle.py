from graphics import *

class circle():
      def draw_circle(xc,yc,r,win,color):
          
          x=0
          y=r
          pk=1-r
          while(x<=y):
               dibujarPixel1=Point( xc+x,yc+y);
               dibujarPixel1.setFill(color)            
               dibujarPixel2=Point( xc-x,yc+y);
               dibujarPixel2.setFill(color) 
               dibujarPixel3=Point( xc+x,yc-y);
               dibujarPixel3.setFill(color)             
               dibujarPixel4=Point( xc-x,yc-y); 
               dibujarPixel4.setFill(color)  			
               dibujarPixel5=Point( yc+y,xc+x);
               dibujarPixel5.setFill(color)              
               dibujarPixel6=Point( yc-y,xc+x);
               dibujarPixel6.setFill(color)      
               dibujarPixel7=Point( yc+y,xc-x);
               dibujarPixel7.setFill(color)              
               dibujarPixel8=Point( yc-y,xc-x);
               dibujarPixel8.setFill(color)     
               
               dibujarPixel1.draw(win) 
               dibujarPixel2.draw(win)
               dibujarPixel3.draw(win)
               dibujarPixel4.draw(win)
               dibujarPixel5.draw(win)
               dibujarPixel6.draw(win)
               dibujarPixel7.draw(win)
               dibujarPixel8.draw(win)
               
               win.plotPixel(100,100,color)

               if pk<0:
                   pk+=2*(x+1)+1
                   x+=1
               else:
                    pk=3*(x+1)+1-2*(y-1)
                    x+=1
                    y-=1     
                    
      def create(color):                   
          win = GraphWin("Circulo", 250, 200)
          circle.draw_circle(100,100,50,win,color)
          for i in range(200):
              circle.draw_circle(100,100,50-i,win,color)
          win.getMouse()
          win.close()
