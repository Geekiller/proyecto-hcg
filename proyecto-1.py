#christian kuchler
#Ariel flores

import tkinter as tk
from tkinter import*
from tkinter import ttk
from tkinter.messagebox import showinfo

#window configurations
ventana=tk.Tk()
ventana.geometry("610x410")
ventana.eval('tk::PlaceWindow . center')
ventana.configure(bg='#48608A')
ventana.title("Proyecto#1 Herramientas de Computación Gráfica")
selected_color = tk.StringVar()
selected_figure = tk.StringVar()



def select_colors(event):
    msg = f'You selected {colores.get()}!'
    showinfo(title='Result', message=msg)


def select_figures(event):
    msg = f'You selected {fig.get()}!'
    showinfo(title='Result', message=msg)



img = PhotoImage(file='proyecto#1/image.png')
img2 = PhotoImage(file='proyecto#1/image2.png')



def select_form():
    palabra=selected_figure.get()
    color=selected_color.get()
    print(color)
    print(palabra)
    if(palabra=="Cuadrado"):
         from d_box import box
         draw_form1=box.create(color)

    elif(palabra=="Circulo"):
        from d_circle import circle
        draw_form2=circle.create(color)

    elif(palabra=="Triangulo"):
        from d_triangle_r import Triangle
        draw_form3=Triangle.create(color)




#etiquetas 
etq1=tk.Label(text="",bg="#4F7B82").place(x=30,y=30,height=350,width=550)
etq2=tk.Label(text="Escoge la figura que deseas dibujar",bg="#4F7B82",font=('arial' ,16,'bold')).place(x=150,y=110,height=30,width=420)
etq3=tk.Label(text="",bg="white").place(x=190,y=150,height=30,width=300)


#combobox para figuras
fig= ttk.Combobox(ventana, textvariable=selected_figure)
fig['values'] =figuras = ('Cuadrado', 'Circulo', 'Triangulo')
fig['state'] = 'readonly'  # normal
fig.place(x=190,y=150,height=30,width=300)


#Combobox para colores
colores= ttk.Combobox(ventana, textvariable=selected_color)
colores['values'] = colors = ('green', 'blue', 'yellow', 'red', 'pink','purple','cyan','orange','aqua','beige','deepskyblue')
colores['state'] = 'readonly'  # normal
colores.place(x=190,y=190,height=30,width=300)


#etiquetas de imagenes
image1 = Label(ventana, image=img,bg="#4F7B82").place(x=100,y=80,height=180,width=80)
image2 = Label(ventana, image=img2,bg="#4F7B82").place(x=500,y=185,height=45,width=50)

#boton para tibujar
btn1=tk.Button(ventana,text="Dibujar",font=('arial' ,20,'bold'),command=select_form).place(x=390,y=250,height=40,width=105)

ventana.mainloop()


